﻿using InSky.Application.Common.Wrappers;
using InSky.Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InSky.Application.Contacts.Queries.GetAllContacts
{
    public class GetAllContactsQuery : IRequest<AllContactsResponce<IEnumerable<GetAllContactsVM>>>
    {
    }
    public class GetAllContactsQueryHandler : IRequestHandler<GetAllContactsQuery, AllContactsResponce<IEnumerable<GetAllContactsVM>>>
    {
        private readonly IContactServiceAsync _contactService;
        public GetAllContactsQueryHandler(IContactServiceAsync contactService)
        {
            _contactService = contactService;

        }

        public async Task<AllContactsResponce<IEnumerable<GetAllContactsVM>>> Handle(GetAllContactsQuery request, CancellationToken cancellationToken)
        {

            return null;
        }


    }
}
