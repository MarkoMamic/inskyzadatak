﻿using InSky.Application.Common.Exceptions;
using InSky.Application.Common.Wrappers;
using MediatR;
using InSky.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InSky.Application.Interfaces;

namespace InSky.Application.Contacts.Queries.GetDetailedContact
{
    public class GetDetailedContactByIdQuery : IRequest<Response<Contact>>
    {
        public int Id { get; set; }
        public class GetContactByIdQueryHandler : IRequestHandler<GetDetailedContactByIdQuery, Response<Contact>>
        {
            private readonly IContactServiceAsync _contactService;
            public GetContactByIdQueryHandler(IContactServiceAsync contactService)
            {
                _contactService = contactService;
            }
            public async Task<Response<Contact>> Handle(GetDetailedContactByIdQuery query, CancellationToken cancellationToken)
            {
                return null;

                var contact = await _contactService.GetByIdAsync(query.Id);
                if (contact == null) throw new ApiException($"Contact Not Found.");
                return new Response<Contact>(contact);
            }
        }
    }
}
