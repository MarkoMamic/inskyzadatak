﻿using InSky.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace InSky.Application.Interfaces
{
    public interface IContactServiceAsync : IGenericServiceAsync<Contact>
    {
    }
}
