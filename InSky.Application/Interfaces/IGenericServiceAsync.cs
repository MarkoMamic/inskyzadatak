﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InSky.Application.Interfaces
{
    public interface IGenericServiceAsync<T> where T : class
    {
        Task<T> GetByIdAsync(int id);
        Task<IReadOnlyList<T>> GetAllAsync();

    }
}
