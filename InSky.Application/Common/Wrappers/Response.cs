﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InSky.Application.Common.Wrappers
{
    public class AllContactsResponce<T>
    {
        public AllContactsResponce()
        {
        }
        public AllContactsResponce(T data, string message = null)
        {
            Succeeded = true;
            Message = message;
            Data = data;
        }
        public AllContactsResponce(string message)
        {
            Succeeded = false;
            Message = message;
        }
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public List<string> Errors { get; set; }
        public T Data { get; set; }
    }
}
