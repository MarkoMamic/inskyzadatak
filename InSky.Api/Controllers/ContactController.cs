﻿using InSky.Application.Contacts.Queries.GetAllContacts;
using InSky.Application.Contacts.Queries.GetDetailedContact;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net;

namespace InSky.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : BaseApiController
    {


        [HttpGet("getall")]
        public async Task<IActionResult> GetAll()
        {

            return Ok(await Mediator.Send(new GetAllContactsQuery()));
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetDetailedContactByIdQuery { Id = id }));
        }

    }
}
