﻿using InSky.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InSky.Infrastructure.MicrosoftDynamic365Connector.Services
{
    public class GenericServiceAsync<T> : IGenericServiceAsync<T> where T : class
    {
        public Task<IReadOnlyList<T>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
