﻿using InSky.Application.Interfaces;
using InSky.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InSky.Infrastructure.MicrosoftDynamic365Connector.Services
{
    public class ContactServiceAsync : GenericServiceAsync<Contact>, IContactServiceAsync
    {

    }
}
