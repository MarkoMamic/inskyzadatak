﻿using InSky.Application.Interfaces;
using InSky.Domain.Settings;
using InSky.Infrastructure.MicrosoftDynamic365Connector.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace InSky.Infrastructure.MicrosoftDynamic365Connector
{
    public static class ServiceExtensions
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Credentials>(configuration.GetSection("Credentials"));
            services.AddTransient(typeof(IGenericServiceAsync<>), typeof(GenericServiceAsync<>));
            services.AddTransient<IContactServiceAsync, ContactServiceAsync>();
        }
    }
}
