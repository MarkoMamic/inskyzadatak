﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InSky.Domain.Entity
{
    public class Contact
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Description { get; set; }
    }
}
