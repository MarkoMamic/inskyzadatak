﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InSky.Domain.Settings
{
    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
    }
}
